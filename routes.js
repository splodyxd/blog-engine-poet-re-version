var html2text = require('html-to-text');

module.exports = function (app) {

	app.get('/', function (req, res) {	
		res.render('index', {
			title: 'SplodyCode.net'
		});
	});

	app.get('/tags', function (req, res) {
		res.render('tags', {
			title: 'Tags | SplodyCode'
		});
	});

	app.get('/categories', function (req, res) {
		res.render('categories', {
			title: 'Categories | SplodyCode'
		});
	});

	app.get('/rss', function (req, res) {
		var posts = app.locals.getPosts(0, 5);
		res.setHeader('Content-Type', 'application/rss+xml');
		res.render('rss', { posts: posts });
	});
};
