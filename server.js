var express = require('express'),
	app = express(),
	Poet = require('poet'),
	schedule = require('node-schedule'),
	handle404 = require('./lib/handle-404');

var poet = Poet(app, {
	postsPerPage: 10,
	posts: __dirname + '/_posts',
	metaFormat: 'json',
});

poet
	.watch()
	.init();

app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(express.static(__dirname + '/public'));
app.use(app.router);
app.use(handle404);

app.configure('development', function () {
	app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
	app.locals.pretty = true;
});

app.configure('production', function () {
	app.use(express.errorHandler());
	app.locals.pretty = true;
});

require('./routes')(app);

app.listen(8081);
